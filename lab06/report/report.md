# Rapport HPC - Laboratoire 6 - Bacso Gaetan

## 1er essais

### Optimisation

Modification des boucles for pour qu'elles utilisent mieux le cache. Accès en row major. Modification effectuée dans `sobel_filter()`, `gaussian_filter()` et `rgb_to_grayscale()`.

Cette modification rend le code plus efficace car en effet le prefetcher va sûrement charger plusieurs lignes de la matrice en cache au lieu d'une et donc on aura moins souvent besoin d'aller chercher les données dans la mémoire.

#### Exemple

##### Version de base

```c
for (size_t x = 0; x < img->width; x++) {
    for (size_t y = 0; y < img->height; y++) {
        // some code
    }
}
```

##### Version modifiée

```c
for (size_t y = 0; y < img->height; y++) {
    for (size_t x = 0; x < img->width; x++) {
        // some code
    }
}
```

### Résultat

#### Sans modification

![](img/1_bench_none.png)

#### Avec modification

![](img/1_bench_with.png)

#### Conclusion

On peut voir que pour les images `medalion.png` et `half-life.png`, la différence n'est pas flagrante. En revanche pour l'image `stars.png`, on peut voir une nette amélioration. Avec un rapide calcule, on peut voir que le temps observé pour cette image a diminué de 70 - 80%.

## 2ème essais

### Optimisation

Suppression des divisions de multiple de 2 par des décalages à droite. Cela devrait permettre de ne pas avoir d'instruction trop longues en temps.

#### Exemple

##### Sans modification

```c
SOBEL_KERNEL_SIZE/2
```

##### Avec modification

```c
SOBEL_KERNEL_SIZE>>1
```

### Résultat

#### Sans modification

![](img/1_bench_with.png)

#### Avec modification

![](img/2_bench_with.png)

#### Conclusion

On retrouve exactement les mêmes temps d’exécution. Cela est sûrement du au fait que le compilateur avait déjà optimisé ces instructions.

## 3ème essais

### Optimisation

J'ai essayé de pré calculer les index des tableaux pour que le compilateur essaye de les optimiser. 

#### Exemple

##### Sans modification

```c
kernel[ky * GAUSSIAN_KERNEL_SIZE + kx]
```

##### Avec modification

```c
const int idxK = ky * GAUSSIAN_KERNEL_SIZE + kx;
kernel[idxK]
```

### Résultat

#### Sans modification

![](img/2_bench_with.png)

#### Avec modification

![](img/3_bench_with.png)

#### Conclusion

On peut voir que les temps sont moins bon. Je ne comprends pas trop pourquoi. J'imagine que le compilateur n'a pas réussi à faire mieux ou que sont optimisation était au final moins adaptée à notre cas.

## 4ème essais

### Optimisation

J'ai essayé d'enlever la condition dans les boucles for pour diminuer le nombre de saut conditionnel.

#### Exemple

##### Sans modification

```c
if (x < (SOBEL_KERNEL_SIZE>>1) ||
    x >= img->width - (SOBEL_KERNEL_SIZE>>1) ||
    y < (SOBEL_KERNEL_SIZE>>1) ||
    y >= img->height - (SOBEL_KERNEL_SIZE>>1)) {
        res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
        continue;
}
```

##### Avec modification

```c
/*if (x < (SOBEL_KERNEL_SIZE>>1) ||
    x >= img->width - (SOBEL_KERNEL_SIZE>>1) ||
    y < (SOBEL_KERNEL_SIZE>>1) ||
    y >= img->height - (SOBEL_KERNEL_SIZE>>1)) {
        res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
        continue;
}*/
```

### Résultat

#### Sans modification

![](img/4_bench_without.png)

#### Avec modification

![](img/4_bench_with.png)

#### Conclusion

On peut voir que au final les fonction ``gaussian_filter`` et ``sobel_filter`` sont moins efficaces. 

## 5ème essais

### Optimisation

Parcours du kernel pour sobel et gaussian de manière 1D.

#### Exemple

##### Sans modification

```c
for (size_t ky = 0; ky < SOBEL_KERNEL_SIZE; ky++) {
    for (size_t kx = 0; kx < SOBEL_KERNEL_SIZE; kx++) {
        // some code
    }
}
```

##### Avec modification

```c
for (size_t ky = 0; ky < SOBEL_KERNEL_SIZE<<1; ky++) {

}
```

### Résultat

#### Sans modification

<img title="" src="img/4_bench_with.png" alt="" data-align="inline">

#### Avec modification

![](img/5_bench_with.png)

#### Conclusion

On peut voir que la vitesse d'execution de `gaussian_filter` et `sobel_filter` est environ diminuée par deux ce qui est une amélioration notable.

## 6ème essais

### Optimisation

Parcours de l'image de manière 1D.

#### Exemple

##### Sans modification

```c
for (size_t y = 0; y < img->height; y++) {
    for (size_t x = 0; x < img->width; x++) {
        // some code
    }
}
```

##### Avec modification

```c
for (size_t y = 0; y < img->height*img->width; y++) {

}
```

### Résultat

#### Sans modification

![](img/5_bench_with.png)

#### Avec modification

![](img/6_bench_with.png)

#### Conclusion

On peut voir que la vitesse d'execution de `gaussian_filter`, `sobel_filter` et `rgb_to_grayscale` est un peu diminuée ce qui est une amélioration.

## Conclusion finale

Lors de ce laboratoire j'ai pû découvrir plusieurs manière d'optimiser un programme et j'ai aussi pu remarquer que certaines idées n'étaient pas forcément bonne ou n'apportaient pas grand chose (surement du aux optimisation du compilateur).

En finalité j'ai quand même réussi à diminuer le temps du programme de base d'environ 88%. 

$$
(128269us/1000456us)*100 = 12.82\%
$$

$$
100 - 12.82 ≃ 88\%
$$


