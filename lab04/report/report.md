# Rapport - Bacso Gaetan - Lab04

## Array

### Perf

```bash
sudo perf report --stdio --dsos=sort
# To display the perf.data header info, please use --header/--header-only options.
#
# dso: sort
#
# Total Lost Samples: 0
#
# Samples: 52K of event 'cpu-clock'
# Event count (approx.): 13067250000
#
# Overhead  Command  Symbol        
# ........  .......  ..............
#
    99.97%  sort     [.] array_sort
     0.01%  sort     [.] array_init


# Samples: 9  of event 'faults'
# Event count (approx.): 298
#
# Overhead  Command  Symbol        
# ........  .......  ..............
#
    72.15%  sort     [.] array_init


#
# (Tip: Use parent filter to see specific call path: perf report -p <regex>)
#
```



```assembly
sudo perf annotate --stdio --dsos=sort --symbol=array_sort            
 Percent |      Source code & Disassembly of sort for cpu-clock (52252 samples, percent: local period)
------------------------------------------------------------------------------------------------------
         :
         :
         :
         :          Disassembly of section .text:
         :
         :          0000000000001430 <array_sort>:
         :          array_sort():
         :          *yp = temp;
         :          }
         :
         :          /* Arrange a array in increasing order of value */
         :          // https://www.geeksforgeeks.org/bubble-sort/
         :          void array_sort(uint64_t *data, const size_t len){
    0.00 :   1430:   push   %rbp
    0.00 :   1431:   mov    %rsp,%rbp
    0.00 :   1434:   callq  *0x2bae(%rip)        # 3fe8 <mcount@GLIBC_2.2.5>
         :          size_t i, j;
         :          for (i = 0; i < len-1; i++){
    0.00 :   143a:   sub    $0x1,%rsi
    0.00 :   143e:   mov    %rsi,%r8
    0.00 :   1441:   je     1481 <array_sort+0x51>
    0.00 :   1443:   lea    (%rdi,%rsi,8),%rsi
    0.00 :   1447:   nopw   0x0(%rax,%rax,1)
         :          void array_sort(uint64_t *data, const size_t len){
    0.00 :   1450:   mov    %rdi,%rax
    0.00 :   1453:   nopl   0x0(%rax,%rax,1)
         :          // Last i elements are already in place
         :          for (j = 0; j < len-i-1; j++) {
         :          if (data[j] > data[j+1]) {
    4.57 :   1458:   mov    (%rax),%rdx
   29.76 :   145b:   mov    0x8(%rax),%rcx
    1.00 :   145f:   cmp    %rcx,%rdx
    0.00 :   1462:   jbe    146e <array_sort+0x3e>
         :          array_swap():
         :          *yp = temp;
   17.15 :   1464:   movslq %edx,%rdx
         :          *xp = *yp;
    9.45 :   1467:   mov    %rcx,(%rax)
         :          *yp = temp;
    6.22 :   146a:   mov    %rdx,0x8(%rax)
         :          array_sort():
         :          for (j = 0; j < len-i-1; j++) {
   22.62 :   146e:   add    $0x8,%rax
    9.23 :   1472:   cmp    %rax,%rsi
    0.00 :   1475:   jne    1458 <array_sort+0x28>
         :          for (i = 0; i < len-1; i++){
    0.00 :   1477:   sub    $0x8,%rsi
    0.00 :   147b:   sub    $0x1,%r8
    0.00 :   147f:   jne    1450 <array_sort+0x20>
         :          array_swap(&data[j], &data[j+1]);
         :          }
         :          }
         :          }
         :          }
    0.00 :   1481:   pop    %rbp
    0.00 :   1482:   retq
```

### Cachgring

```bash
valgrind --tool=callgrind --simulate-cache=yes ./sort array 10000
==43046== Callgrind, a call-graph generating cache profiler
==43046== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==43046== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==43046== Command: ./sort array 10000
==43046== 
--43046-- warning: L3 cache found, using its data for the LL simulation.
==43046== For interactive control, run 'callgrind_control -h'.
==43046== 
==43046== Process terminating with default action of signal 27 (SIGPROF)
==43046==    at 0x49795CA: __open_nocancel (in /usr/lib/libc-2.31.so)
==43046==    by 0x498545F: write_gmon (in /usr/lib/libc-2.31.so)
==43046==    by 0x4985C1D: _mcleanup (in /usr/lib/libc-2.31.so)
==43046==    by 0x48C2536: __run_exit_handlers (in /usr/lib/libc-2.31.so)
==43046==    by 0x48C26ED: exit (in /usr/lib/libc-2.31.so)
==43046==    by 0x48AB029: (below main) (in /usr/lib/libc-2.31.so)
==43046== 
==43046== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==43046== Collected : 425106050 100213835 49560185 958 3709536 2246 949 2081 1891
==43046== 
==43046== I   refs:      425,106,050
==43046== I1  misses:            958
==43046== LLi misses:            949
==43046== I1  miss rate:        0.00%
==43046== LLi miss rate:        0.00%
==43046== 
==43046== D   refs:      149,774,020  (100,213,835 rd + 49,560,185 wr)
==43046== D1  misses:      3,711,782  (  3,709,536 rd +      2,246 wr)
==43046== LLd misses:          3,972  (      2,081 rd +      1,891 wr)
==43046== D1  miss rate:         2.5% (        3.7%   +        0.0%  )
==43046== LLd miss rate:         0.0% (        0.0%   +        0.0%  )
==43046== 
==43046== LL refs:         3,712,740  (  3,710,494 rd +      2,246 wr)
==43046== LL misses:           4,921  (      3,030 rd +      1,891 wr)
==43046== LL miss rate:          0.0% (        0.0%   +        0.0%  )
[1]    43046 profile signal  valgrind --tool=callgrind --simulate-cache=yes ./sort array 10000
```

### Callgrind

```c
callgrind_annotate --auto=yes --show-percs=yes callgrind.out.42940  
--------------------------------------------------------------------------------
Profile data file 'callgrind.out.42940' (creator: callgrind-3.15.0)
--------------------------------------------------------------------------------
I1 cache: 
D1 cache: 
LL cache: 
Timerange: Basic block 0 - 100215223
Trigger: Program termination
Profiled target:  ./sort array 10000 (PID 42940, part 1)
Events recorded:  Ir
Events shown:     Ir
Event sort order: Ir
Thresholds:       99
Include dirs:     
User annotated:   
Auto-annotation:  on

--------------------------------------------------------------------------------
Ir                   
--------------------------------------------------------------------------------
425,410,025 (100.0%)  PROGRAM TOTALS

--------------------------------------------------------------------------------
Ir                    file:function
--------------------------------------------------------------------------------
424,471,174 (99.78%)  array_util.c:array_sort [/home/gaetan/Documents/HEIG-VD/HPC/Labos/hpc20_student/lab04/code/src/sort]

--------------------------------------------------------------------------------
-- Auto-annotated source: array_util.c
--------------------------------------------------------------------------------
Ir                   

-- line 2 ----------------------------------------
          .           #include <time.h>
          .           #include <stdlib.h>
          .           #include <stdint.h>
          .           #include <stdio.h>
          .           #include "array_util.h"
          .           /* Allocate an array of size "len" and fill it
          .            * with random data.
          .            * Return the array pointer */
          8 ( 0.00%)  uint64_t *array_init(const size_t len){
         31 ( 0.00%)  => ???:mcount (1x)
          9 ( 0.00%)      uint64_t * array = (uint64_t *) malloc(len*sizeof(uint64_t));
        851 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
          .               size_t i;
         14 ( 0.00%)      srand(time(NULL));
     11,789 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (2x)
     20,005 ( 0.00%)      for(i = 0; i < len; i++){
    130,004 ( 0.03%)          array[i] = rand() % 100;
    559,300 ( 0.13%)  => ???:rand (9,999x)
        668 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
          .               }
          .               return array;
          7 ( 0.00%)  }
          .           
          .           /* Liberate array memory */
          3 ( 0.00%)  void array_clear(uint64_t *data){
         31 ( 0.00%)  => ???:mcount (1x)
          6 ( 0.00%)      free(data);
        748 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
          1 ( 0.00%)  }
          .           
          .           // https://www.geeksforgeeks.org/bubble-sort/
          .           void array_swap(uint64_t *xp, uint64_t *yp)  
          .           {  
          .               int temp = *xp;  
 24,818,723 ( 5.83%)      *xp = *yp;  
 49,637,446 (11.67%)      *yp = temp;  
          .           } 
          .           
          .           /* Arrange a array in increasing order of value */
          .           // https://www.geeksforgeeks.org/bubble-sort/
     20,001 ( 0.00%)  void array_sort(uint64_t *data, const size_t len){
         31 ( 0.00%)  => ???:mcount (1x)
          .               size_t i, j;  
     30,002 ( 0.01%)      for (i = 0; i < len-1; i++){
          .                   // Last i elements are already in place  
149,985,000 (35.26%)          for (j = 0; j < len-i-1; j++) { 
199,980,000 (47.01%)              if (data[j] > data[j+1]) { 
          .                           array_swap(&data[j], &data[j+1]);
          .                       }
          .                   }
          .               }      
          2 ( 0.00%)  }
          .           
          .           void array_print(uint64_t *data, const size_t len){
          .               size_t i;
          .               printf("Print Array ------------------------------\n"); 
          .               for(i = 0; i < len; i++){
          .                   printf("%ld ", data[i]); 
          .               }
          .               printf("\n");
-- line 53 ----------------------------------------

--------------------------------------------------------------------------------
Ir                   
--------------------------------------------------------------------------------
424,621,231 (99.81%)  events annotated

```

### Analyse des résultats

Dans perf et callgrind, on peut clairement voir que les hotspot du programme sont la fonction array_sort et array_swap. 

Array_sort comporte deux parties problématique. Ces deux parties sont la deuxième boucle for et la comparaison qui se trouve dans le if de la boucle.

On peut voir avec perf et cachegring que les cache miss se trouvent principalement dans la fonction d'init ce qui est normal car nous allouons la mémoire.

Les fonctions à améliorer sont donc array_sort et array_swap. On peut également remarquer que nous sommes dans un cas de cpu et memory bound.

### Amélioration possible

#### Array_sort

Il est possible d'optimiser la fonction de sort en arrêtant l'algorithme lorsque la deuxième boucle ne fait plus de swap. Cela veut dire que le tableau est déjà trié. Il est aussi possible de jouer avec les index des deux boucle for pour ne pas avoir de calcul inutile.

Exemple d'un bubble sort optimisé (source : https://cboard.cprogramming.com/c-programming/32885-optimizing-bubble-sort.html)

```c
void bubble_sort(int A[], int n)
{
    int i, j;
    int clean_pass = 0;
     
    for (i = 0; !clean_pass && i < n - 1; ++i) {
        clean_pass = 1;
        for (j = i + 1; j < n; ++j) {
            if (A[i] > A[j]) {
                int t = A[i];
                A[i] = A[j];
                A[j] = t;
                clean_pass = 0;
            }
 
        }
    }
}
```

Il serait aussi possible d'utiliser un autre algorithme de tri qui est mieux adapté.

#### Array_swap

Il serait possible d'utiliser XCHG comme vu dans le laboratoire précédent pour optimiser le swap. Je n'ai pas trouvé d'autre solution car en cherchant sur stackoverflow, j'ai pu m'apercevoir que la méthode que j'utilise est déjà une des meilleurs implémentation. Pour ce faire il serait possible d'utiliser les paramètre d'optimisation (-O) de gcc.

## List

### Perf

```bash
sudo perf report --stdio --dsos=sort                             
# To display the perf.data header info, please use --header/--header-only options.
#
# dso: sort
#
# Total Lost Samples: 0
#
# Samples: 57K of event 'cpu-clock'
# Event count (approx.): 14476500000
#
# Overhead  Command  Symbol       
# ........  .......  .............
#
    99.94%  sort     [.] list_sort
     0.01%  sort     [.] list_init
     0.00%  sort     [.] rand@plt


# Samples: 13  of event 'faults'
# Event count (approx.): 855
#
# Overhead  Command  Symbol
# ........  .......  ......
#


#
# (Tip: To record every process run by a user: perf record -u <user>)
#
```

```assembly
sudo perf annotate --stdio --dsos=sort --symbol=list_sort            
 Percent |      Source code & Disassembly of sort for cpu-clock (57871 samples, percent: local period)
------------------------------------------------------------------------------------------------------
         :
         :
         :
         :          Disassembly of section .text:
         :
         :          0000000000001650 <list_sort>:
         :          list_sort():
         :          b->val = temp;
         :          }
         :
         :          // https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
         :          /* Arrange a list in increasing order of value */
         :          void list_sort(struct list_element *head){
    0.00 :   1650:   push   %rbp
    0.00 :   1651:   mov    %rsp,%rbp
    0.00 :   1654:   callq  *0x298e(%rip)        # 3fe8 <mcount@GLIBC_2.2.5>
         :          size_t swapped;
         :          struct list_element *ptr1;
         :          struct list_element *lptr = NULL;
         :
         :          /* Checking for empty list */
         :          if (head == NULL)
    0.00 :   165a:   test   %rdi,%rdi
    0.00 :   165d:   je     16bd <list_sort+0x6d>
         :          do
         :          {
         :          swapped = 0;
         :          ptr1 = head;
         :
         :          while (ptr1->next != lptr)
    0.00 :   165f:   mov    0x8(%rdi),%r11
         :          struct list_element *lptr = NULL;
    0.00 :   1663:   xor    %r9d,%r9d
         :          while (ptr1->next != lptr)
    0.00 :   1666:   cmp    %r11,%r9
    0.00 :   1669:   je     16bd <list_sort+0x6d>
    0.00 :   166b:   nopl   0x0(%rax,%rax,1)
    0.00 :   1670:   mov    (%rdi),%esi
    0.01 :   1672:   mov    %rdi,%r8
    0.00 :   1675:   mov    %r11,%rax
         :          swapped = 0;
    0.00 :   1678:   xor    %r10d,%r10d
    0.00 :   167b:   jmp    169a <list_sort+0x4a>
    0.00 :   167d:   nopl   (%rax)
         :          while (ptr1->next != lptr)
   16.78 :   1680:   mov    0x8(%rax),%rcx
         :          list_swap():
         :          a->val = b->val;
   12.00 :   1684:   mov    %edx,(%r8)
         :          b->val = temp;
    3.38 :   1687:   mov    %esi,(%rax)
         :          list_sort():
         :          while (ptr1->next != lptr)
    0.03 :   1689:   cmp    %rcx,%r9
    0.00 :   168c:   je     16b5 <list_sort+0x65>
         :          {
         :          if (ptr1->val > ptr1->next->val)
         :          {
         :          list_swap(ptr1, ptr1->next);
         :          swapped = 1;
    1.08 :   168e:   mov    $0x1,%r10d
         :          swapped = 0;
    2.89 :   1694:   mov    %rax,%r8
    3.43 :   1697:   mov    %rcx,%rax
         :          if (ptr1->val > ptr1->next->val)
    1.79 :   169a:   mov    (%rax),%edx
   24.27 :   169c:   cmp    %esi,%edx
    0.00 :   169e:   jl     1680 <list_sort+0x30>
         :          while (ptr1->next != lptr)
   17.41 :   16a0:   mov    0x8(%rax),%rcx
   12.50 :   16a4:   cmp    %r9,%rcx
    0.00 :   16a7:   je     16b0 <list_sort+0x60>
    4.43 :   16a9:   mov    %edx,%esi
    0.00 :   16ab:   jmp    1694 <list_sort+0x44>
    0.00 :   16ad:   nopl   (%rax)
         :          }
         :          ptr1 = ptr1->next;
         :          }
         :          lptr = ptr1;
         :          }
         :          while (swapped);
    0.00 :   16b0:   test   %r10,%r10
    0.00 :   16b3:   je     16bd <list_sort+0x6d>
         :          struct list_element *lptr = NULL;
    0.00 :   16b5:   mov    %rax,%r9
         :          while (ptr1->next != lptr)
    0.00 :   16b8:   cmp    %r11,%r9
    0.00 :   16bb:   jne    1670 <list_sort+0x20>
         :          }
    0.00 :   16bd:   pop    %rbp
    0.00 :   16be:   retq

```

### Cachgring

```bash
valgrind --tool=callgrind --simulate-cache=yes ./sort list 10000                  
==43616== Callgrind, a call-graph generating cache profiler
==43616== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==43616== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==43616== Command: ./sort list 10000
==43616== 
--43616-- warning: L3 cache found, using its data for the LL simulation.
==43616== For interactive control, run 'callgrind_control -h'.
==43616== 
==43616== Process terminating with default action of signal 27 (SIGPROF)
==43616==    at 0x49795CA: __open_nocancel (in /usr/lib/libc-2.31.so)
==43616==    by 0x498545F: write_gmon (in /usr/lib/libc-2.31.so)
==43616==    by 0x4985C1D: _mcleanup (in /usr/lib/libc-2.31.so)
==43616==    by 0x48C2536: __run_exit_handlers (in /usr/lib/libc-2.31.so)
==43616==    by 0x48C26ED: exit (in /usr/lib/libc-2.31.so)
==43616==    by 0x48AB029: (below main) (in /usr/lib/libc-2.31.so)
==43616== 
==43616== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==43616== Collected : 528577532 100872290 49872303 956 24380246 6203 946 2083 5641
==43616== 
==43616== I   refs:      528,577,532
==43616== I1  misses:            956
==43616== LLi misses:            946
==43616== I1  miss rate:        0.00%
==43616== LLi miss rate:        0.00%
==43616== 
==43616== D   refs:      150,744,593  (100,872,290 rd + 49,872,303 wr)
==43616== D1  misses:     24,386,449  ( 24,380,246 rd +      6,203 wr)
==43616== LLd misses:          7,724  (      2,083 rd +      5,641 wr)
==43616== D1  miss rate:        16.2% (       24.2%   +        0.0%  )
==43616== LLd miss rate:         0.0% (        0.0%   +        0.0%  )
==43616== 
==43616== LL refs:        24,387,405  ( 24,381,202 rd +      6,203 wr)
==43616== LL misses:           8,670  (      3,029 rd +      5,641 wr)
==43616== LL miss rate:          0.0% (        0.0%   +        0.0%  )
[1]    43616 profile signal  valgrind --tool=callgrind --simulate-cache=yes ./sort list 10000
```



### Callgrind

```c
callgrind_annotate --auto=yes --show-percs=yes callgrind.out.43637
--------------------------------------------------------------------------------
Profile data file 'callgrind.out.43637' (creator: callgrind-3.15.0)
--------------------------------------------------------------------------------
I1 cache: 
D1 cache: 
LL cache: 
Timerange: Basic block 0 - 125917328
Trigger: Program termination
Profiled target:  ./sort list 10000 (PID 43637, part 1)
Events recorded:  Ir
Events shown:     Ir
Event sort order: Ir
Thresholds:       99
Include dirs:     
User annotated:   
Auto-annotation:  on

--------------------------------------------------------------------------------
Ir                   
--------------------------------------------------------------------------------
528,692,591 (100.0%)  PROGRAM TOTALS

--------------------------------------------------------------------------------
Ir                    file:function
--------------------------------------------------------------------------------
524,683,234 (99.24%)  list_util.c:list_sort [/home/gaetan/Documents/HEIG-VD/HPC/Labos/hpc20_student/lab04/code/src/sort]

--------------------------------------------------------------------------------
-- Auto-annotated source: list_util.c
--------------------------------------------------------------------------------
Ir                   

-- line 9 ----------------------------------------
          .           struct list_element{
          .               int val;
          .               struct list_element * next;
          .           };
          .           
          .           /* Allocate "len" linked elements and initialize them
          .            * with random data.
          .            * Return list head */
         10 ( 0.00%)  struct list_element *list_init(size_t len){
         31 ( 0.00%)  => ???:mcount (1x)
          .               size_t i;
          8 ( 0.00%)      struct list_element * head = (struct list_element*) malloc(sizeof(struct list_element));
        854 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
          .               struct list_element * actual = head;
         14 ( 0.00%)      srand(time(NULL));
     11,789 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (2x)
         15 ( 0.00%)      actual->val = rand() % 100;
        668 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
     30,002 ( 0.01%)      for(i = 1; i < len; i++){
     59,994 ( 0.01%)          actual->next = (struct list_element*) malloc(sizeof(struct list_element));
  1,930,127 ( 0.37%)  => ???:malloc (9,999x)
          .                   actual = actual->next;
    109,989 ( 0.02%)          actual->val = rand() % 100;
    559,300 ( 0.11%)  => ???:rand (9,999x)
          .               }
          1 ( 0.00%)      actual->next = NULL;
          .               return head;
          9 ( 0.00%)  }
          .           
          .           /* Liberate list memory */
          5 ( 0.00%)  void list_clear(struct list_element *head){
         31 ( 0.00%)  => ???:mcount (1x)
          .               struct list_element * actual = head;
     49,997 ( 0.01%)      while(actual->next != NULL){
          .                   actual = actual->next;
     20,002 ( 0.00%)          free(head);
  1,019,674 ( 0.19%)  => ???:free (9,998x)
        679 ( 0.00%)  => ???:_dl_runtime_resolve_xsave (1x)
          .                   head = actual;
          .               }
          3 ( 0.00%)      free(actual);
        102 ( 0.00%)  => ???:free (1x)
          3 ( 0.00%)  }
          .           
          .           // https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
          .           /* function to swap data of two nodes a and b*/
          .           void list_swap(struct list_element *a, struct list_element *b) 
          .           { 
          .               int temp = a->val; 
 24,889,312 ( 4.71%)      a->val = b->val; 
 24,889,312 ( 4.71%)      b->val = temp; 
          .           } 
          .           
          .           // https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
          .           /* Arrange a list in increasing order of value */
          3 ( 0.00%)  void list_sort(struct list_element *head){
         31 ( 0.00%)  => ???:mcount (1x)
          .               size_t swapped; 
          .               struct list_element *ptr1; 
      9,794 ( 0.00%)      struct list_element *lptr = NULL; 
          .             
          .               /* Checking for empty list */
          2 ( 0.00%)      if (head == NULL) 
          .                   return; 
          .             
          .               do
          .               { 
 99,947,770 (18.90%)          swapped = 0; 
          .                   ptr1 = head; 
          .             
200,127,587 (37.85%)          while (ptr1->next != lptr) 
          .                   { 
149,921,655 (28.36%)              if (ptr1->val > ptr1->next->val) 
          .                       {  
          .                           list_swap(ptr1, ptr1->next); 
 24,885,611 ( 4.71%)                  swapped = 1; 
          .                       } 
          .                       ptr1 = ptr1->next; 
          .                   } 
          .                   lptr = ptr1; 
          .               } 
     12,186 ( 0.00%)      while (swapped); 
          2 ( 0.00%)  }
          .           
          .           // https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
          .           void list_print(struct list_element *head){
          .               struct list_element *tmp = head; 
          .               printf("Print List ------------------------------\n"); 
          .               while (tmp!=NULL) 
          .               { 
          .                   printf("%d ", tmp->val); 
-- line 88 ----------------------------------------

--------------------------------------------------------------------------------
Ir                   
--------------------------------------------------------------------------------
524,953,286 (99.29%)  events annotated
```

### Analyse des résultats

Dans perf et callgrind, on peut voir que les deux hotspot sont également la fonction list_sort et list_swap. List_swap est un peu moins problématique que pour les array. On voit remarque cette fois-ci que le temps perdu se trouve dans la deuxième boucle while et dans la comparaison du if.

On peut voir avec perf et cachegring que les cache miss se trouvent principalement dans la fonction d'init ce qui est normal car nous allouons la mémoire.

Les fonctions à améliorer sont donc list_sort et list_swap.  On peut également remarquer que nous sommes dans un cas de cpu et memory bound.

### Amélioration possible

#### List_sort

Il serait aussi possible d'utiliser un autre algorithme de tri qui est mieux adapté.

#### List_swap

Il serait possible d'utiliser XCHG comme vu dans le laboratoire précédent pour optimiser le swap. Je n'ai pas trouvé d'autre solution car en cherchant sur stackoverflow, j'ai pu m'apercevoir que la méthode que j'utilise est déjà une des meilleurs implémentation.

Pour ce faire il serait possible d'utiliser les paramètre d'optimisation (-O) de gcc.