# Rapport HPC - Laboratoire 7 - Bacso Gaëtan

## Optimisation 1

Utilisation de l'instruction `cmove` au lieu d'un if classique. Cette optimisation est une conversion de IF (`-fif-conversion, -if-conversion2`). Elle est disponible à partir de l'option `-O1`.

Lien avec et sans optimisation :

[Compiler Explorer](https://godbolt.org/z/my9E7B)

### Analyse et résultat

Lorsque les optimisation ne sont pas activées, on peut effectivement voir en assembleur que le compilateur va bien faire un if classique en utilisant une comparaison avec un jump ou une affectation suivant le résultat de la comparaison. Par contre en utilisant l'optimisation on peut voir que le compilateur va opter pour l'utilisation d'une instruction `cmove` ce qui lui permet de faire une affectation en fonction d'une condition en une seule fois. Cela permet de réduire le code généré et d'utiliser moins d'instruction. 

### Optimisation développeur

Du point de vue développeur il n'est pas vraiment possible d'optimiser le code vu que celui-ci fait déjà référence à une affectation conditionnelle. En C il n'y a pas d'autre manière de l'écrire.

# Optimisation 2

Optimisation "jump threadings". Cette optimisation vise à réduire les sauts conditionnels dans un code.

Lien avec et sans optimisation :

[Compiler Explorer](https://godbolt.org/z/tw3wss)

## Analyse et résultat

Dans le code que j'ai écrit on peut remarquer que l'on appelle une fonction différente par rapport à une condition différente elle aussi. Il y a par contre un cas où ces deux condition seront vraie "b != 0". Cela aura pour effet d'effectuer deux sauts conditionnels sans optimisation alors qu'il serait préférable de n'en faire qu'un dans ce cas précis. Grâce à l'optimisation `-fthread-jumps` disponible à partir de l'option `-O2`, le compilateur arrivera à détecter ces conditions redondantes et les éviter.

## Optimisation développeur

Il est possible de faire la même chose en code C grâce au instruction `goto`. Voici un exemple de code possible :

[Compiler Explorer](https://godbolt.org/z/3XcuFA)

On peut remarquer que le code sans optimisation est très ressemblant au niveau branchement avec celui qui est optimiser. 

# Optimisation 3

Optimisation "code hosting". Cette optimisation vise à sortir les calculs hors des boucles par exemple si elle sont constante pour ne faire le calcul qu'une fois et pas à chaque itération. Cette option est activée par l'option `-fcode-hoisting` et disponible avec l'option `-O2`.

Lien avec et sans optimisation :

[Compiler Explorer](https://godbolt.org/z/McZ_2t)

## Analyse et résultat

Dans le code que j'ai écrit on peut voir qu'une addition est faite lors de chaque itération d'une boucle for. Or elle ne change jamais. On peut voir que dans la version sans optimisation le compilateur va faire ce qui est décrit mais dans la version avec optimisation le compilateur va calculer cette valeur avant la boucle pour ne faire l'addition qu'une seule fois.

## Optimisation développeur

Il est possible d'effectuer cette optimisation en faisant l'addition hors de la boucle est de juste utiliser le résultat à chaque itération. On peut voir que le code sans optimisation est ressemblant avec celui avec optimisation.

Exemple :

[Compiler Explorer](https://godbolt.org/z/4cquwc)
