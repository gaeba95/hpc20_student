# HPC - Laboratoire 2 - Bacso Gaetan

## Problèmes identifié et réponses aux questions

J'ai pu remarquer que lors de la lecture, l'écriture, l'envoi et la réception, le programme traitait les données octet par octet. Cela n'est pas une bonne idée car a chaque octet il faudra faire un appel système ce qui prend du temps. 

Entre la première exécution du programme et les suivantes sur le même fichier, que remarquez-vous et comment l’expliquez-vous ?

Il y a une différence de vitesse. Cela est dû au fait qu'une partie du fichier sera en cache. 

Est-ce que la taille du fichier a une importance ? 

Non, la taille du fichier ne devrait pas influencer la vitesse de transfert. Elle influencera par contre le temps d'exécution.

## Optimisation

L’optimisation que j'ai effectuée dans mon programme est de mettre en place un buffer dans les fichier client.c et server.c. Cela permet de réduire le nombre d'appel système et donc de gagner du temps.

### Analyse du système

```
----------------------------------------------------------------------
CPU model            : Intel(R) Core(TM) i7-1065G7 CPU @ 1.30GHz
Number of cores      : 8
CPU frequency        : 1285.743 MHz
Total size of Disk   : 735.5 GB (68.0 GB Used)
Total amount of Mem  : 15570 MB (1967 MB Used)
Total amount of Swap : 0 MB (0 MB Used)
System uptime        : 0 days, 0 hour 9 min
Load average         : 0.61, 1.09, 0.76
OS                   : Manjaro Linux 
Arch                 : x86_64 (64 Bit)
Kernel               : 5.4.23-1-MANJARO
----------------------------------------------------------------------
I/O speed(1st run)   : 1.1 GB/s
I/O speed(2nd run)   : 1.2 GB/s
I/O speed(3rd run)   : 1.2 GB/s
Average I/O speed    : 1194.7 MB/s
----------------------------------------------------------------------
```

On peut voir que mon système comporte 16 Go de ram et que la vitesse moyenne d'écriture du SSD est de 1.19 GB/s. Ces deux paramètre sont important à prendre en compte car le logiciel à optimisé utilise principalement ces deux ressources. On sait donc que la vitesse ne dépassera pas les 1 GB et que nous somme limité a 16 GB de ram. La ram peut être critique dans le cas ou l'on décide d'implémenter un énorme buffer. Il est donc intéressant de la prendre en compte.

## Analyse des résultats

### Résultat sans optimisation

```bash
❯ make all FILE_SIZE=8192
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c client.c -o client.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c main.c -o main.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c server.c -o server.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\"  client.o  main.o  server.o  -o main
dd if=/dev/urandom of=srv_data.bin bs=4096 count=8192 iflag=count_bytes
2+0 records in
2+0 records out
8192 bytes (8.2 kB, 8.0 KiB) copied, 0.000278311 s, 29.4 MB/s
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 27 sec and 692947274 ns
[time_report] rate: 295.815390 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true

❯ make all FILE_SIZE=8192
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 7 sec and 661055737 ns
[time_report] rate: 1069.304321 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true

```

On peut voir que lors de la première exécution on a un temps de 27 secondes et que lors de la deuxième avec la même taille on a un temps de 7 secondes.

### Résultat avec optimisation

```bash
❯ make all FILE_SIZE=8192
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c client.c -o client.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c main.c -o main.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c server.c -o server.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\"  client.o  main.o  server.o  -o main
dd if=/dev/urandom of=srv_data.bin bs=4096 count=8192 iflag=count_bytes
2+0 records in
2+0 records out
8192 bytes (8.2 kB, 8.0 KiB) copied, 0.000270578 s, 30.3 MB/s
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 0 sec and 10616220 ns
[time_report] rate: 771649.419473 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true

❯ make all FILE_SIZE=8192
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 0 sec and 5391384 ns
[time_report] rate: 1519461.422151 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true
```

On peut voir que le problème de changement de vitesse entre la première exécution et la deuxième est également présent avec la version optimisée. On remarquera par contre que la vitesse de transfert est bien plus grande par rapport à la version non optimisée. Pour avoir cette vitesse j'ai utilisé un buffer de 4096 ce qui correspond à la taille d'une page.

### Différence de vitesse entre la version optimisée et non avec une même taille de fichier (8192 octets)

```bash
----------------------------------- Version optimisée --------------------------------------
❯ make all FILE_SIZE=8192
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c client.c -o client.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c main.c -o main.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c server.c -o server.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\"  client.o  main.o  server.o  -o main
dd if=/dev/urandom of=srv_data.bin bs=4096 count=8192 iflag=count_bytes
2+0 records in
2+0 records out
8192 bytes (8.2 kB, 8.0 KiB) copied, 0.000270578 s, 30.3 MB/s
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 0 sec and 10616220 ns
[time_report] rate: 771649.419473 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true

----------------------------------- Version non optimisée ----------------------------------
❯ make all FILE_SIZE=8192
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c client.c -o client.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c main.c -o main.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\" -c server.c -o server.o
gcc -g -Wall -Werror -DFILE_SIZE=8192 -DSRV_FILE=\"srv_data.bin\" -DCLIENT_FILE=\"client_data.bin\"  client.o  main.o  server.o  -o main
dd if=/dev/urandom of=srv_data.bin bs=4096 count=8192 iflag=count_bytes
2+0 records in
2+0 records out
8192 bytes (8.2 kB, 8.0 KiB) copied, 0.000278311 s, 29.4 MB/s
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 27 sec and 692947274 ns
[time_report] rate: 295.815390 bytes/sec
cat srv_data.bin | tr '\000-\377' '\001-\377\000' | cmp client_data.bin || true
```

On peut voir que le rate de la version non optimisée est de 296 bytes/sec alors que la version optimisée à un rate de 771649 bytes/sec. Cela donne une multiplication de la vitesse par environ 2600. Ce qui est déjà une bonne amélioration.

### Différence de vitesse en fonction de la taille du fichier

![](img/rate_file.png)

On peut voir que la vitesse ne change pas énormément en fonction de la taille du fichier.

### Différence de vitesse en fonction de la taille du buffer pour une même taille de fichier

![](img/rate_buffer.png)

On puet voir que plus le buffer est grand, plus la vitesse augmente. Cependant le point le plus rapide que j'ai pu obtenir (pas présent sur ce graphique) est lorsque la taille du buffer est égal à celle du fichier. Au dela cela n'est pas utile car la vitesse n’augmentera plus vu que tout le fichier pourra être contenu dans le buffer. Cette idée peut être intéressante mais comporte une limite qui est la ram. Si le fichier est trop grand on ne pourra pas allouer un buffer assez grand et donc ce ne sera pas efficace. 

## Conclusion

Pour ce programme il faudrait trouver le juste milieu avec la taille du buffer et un rate idéal. 