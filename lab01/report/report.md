# Rapport HPC - Laboratoire 1 - Bacso Gaëtan

## Implémentation

J'ai choisi d'utiliser un Bubble Sort pour l'implémentation des tris car cet algorithme est simple à mettre en place.

J'ai utilisé ces deux implémentations:

Bubble sort for Array : https://www.geeksforgeeks.org/bubble-sort/

Linked list et bubble sort : https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/

## Programme de test

Le programme de test initialise le conteneur, l'affiche non trié, le trie puis l'affiche de nouveau.