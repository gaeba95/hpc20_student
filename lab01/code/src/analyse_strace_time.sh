#!/bin/zsh
cat strace-array.txt | awk ' 
function find_thread(maxInd, threads, threadStrArr, reqThr)
{
    for (ind = 1; ind <= maxInd; ind++)
    {
        if (reqThr == threads[ind])
        {
            printf "...\n%s\n...\n", threadStrArr[ind]
            for ( ; ind < maxInd; ind++)
            {
                threads[ind] = threads[ind + 1]
                threadStrArr[ind]=threadStrArr[ind + 1]
            }
            return reqThr
        }
    }
    return -1
}
{
    thrCnt=0
    totmatchCnt=0
    totTimetaken=0
    syscall_name="sendto"
    while ((getline myline) > 0) {
        found_syscall_finished=0
        resumed_found=match(myline, "<... "syscall_name" resumed>")
        if (resumed_found != 0)
        {
            # Found "<system call> resumed" string in the input line
            split(myline,a," ");
            thread_id=a[1]
            if (thrCnt > 0)
            {
                # Now need to find matching thread (if any) from the unfinished calls
                foundThr=find_thread(thrCnt, threads, threadStrArr, thread_id)
                if (foundThr != -1)
                {
                    # There is a matching unfinished call found in the trace
                    thrCnt--
                    found_syscall_finished=1
                }
            }
        }
        else
        {
            # It is not resumed system call, check if required system call found
            syscall_found = match(myline, syscall_name"\\(4,")  # Please note the 4, ie first parameter is 4
            if (syscall_found != 0)
            {
                # It is required system call
                unfinished_found=match(myline, "<unfinished ...>$")
                if (unfinished_found != 0)
                {
                    # It is an unfinished call, add thread number to the array for search later
                    split(myline,a," ");
                    thrCnt++
                    threadStrArr[thrCnt]=myline
                    threads[thrCnt]=a[1]
                }
                else
                {
                    found_syscall_finished=1
                }
            }
        }
        if (found_syscall_finished != 0)
        {
            # current line contains required system call which is finished, fetch time and add to total time
            printf "%s\n", myline
            n=split(myline,a,"[ <>]");
            time_took=a[n-1]
            totmatchCnt++
            totTimetaken=totTimetaken+time_took
        }
    }
    if (totmatchCnt != 0)
    {
        avgTime=totTimetaken/totmatchCnt
        printf "total = %s, cnt = %s, average = %s\n", totTimetaken, totmatchCnt, avgTime
    }
}
'

cat strace-list.txt | awk ' 
function find_thread(maxInd, threads, threadStrArr, reqThr)
{
    for (ind = 1; ind <= maxInd; ind++)
    {
        if (reqThr == threads[ind])
        {
            printf "...\n%s\n...\n", threadStrArr[ind]
            for ( ; ind < maxInd; ind++)
            {
                threads[ind] = threads[ind + 1]
                threadStrArr[ind]=threadStrArr[ind + 1]
            }
            return reqThr
        }
    }
    return -1
}
{
    thrCnt=0
    totmatchCnt=0
    totTimetaken=0
    syscall_name="sendto"
    while ((getline myline) > 0) {
        found_syscall_finished=0
        resumed_found=match(myline, "<... "syscall_name" resumed>")
        if (resumed_found != 0)
        {
            # Found "<system call> resumed" string in the input line
            split(myline,a," ");
            thread_id=a[1]
            if (thrCnt > 0)
            {
                # Now need to find matching thread (if any) from the unfinished calls
                foundThr=find_thread(thrCnt, threads, threadStrArr, thread_id)
                if (foundThr != -1)
                {
                    # There is a matching unfinished call found in the trace
                    thrCnt--
                    found_syscall_finished=1
                }
            }
        }
        else
        {
            # It is not resumed system call, check if required system call found
            syscall_found = match(myline, syscall_name"\\(4,")  # Please note the 4, ie first parameter is 4
            if (syscall_found != 0)
            {
                # It is required system call
                unfinished_found=match(myline, "<unfinished ...>$")
                if (unfinished_found != 0)
                {
                    # It is an unfinished call, add thread number to the array for search later
                    split(myline,a," ");
                    thrCnt++
                    threadStrArr[thrCnt]=myline
                    threads[thrCnt]=a[1]
                }
                else
                {
                    found_syscall_finished=1
                }
            }
        }
        if (found_syscall_finished != 0)
        {
            # current line contains required system call which is finished, fetch time and add to total time
            printf "%s\n", myline
            n=split(myline,a,"[ <>]");
            time_took=a[n-1]
            totmatchCnt++
            totTimetaken=totTimetaken+time_took
        }
    }
    if (totmatchCnt != 0)
    {
        avgTime=totTimetaken/totmatchCnt
        printf "total = %s, cnt = %s, average = %s\n", totTimetaken, totmatchCnt, avgTime
    }
}
'