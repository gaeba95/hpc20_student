
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "array_util.h"
/* Allocate an array of size "len" and fill it
 * with random data.
 * Return the array pointer */
uint64_t *array_init(const size_t len){
    uint64_t * array = (uint64_t *) malloc(len*sizeof(uint64_t));
    size_t i;
    srand(time(NULL));
    for(i = 0; i < len; i++){
        array[i] = rand() % 100;
    }
    return array;
}

/* Liberate array memory */
void array_clear(uint64_t *data){
    free(data);
}

// https://www.geeksforgeeks.org/bubble-sort/
void array_swap(uint64_t *xp, uint64_t *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
} 

/* Arrange a array in increasing order of value */
// https://www.geeksforgeeks.org/bubble-sort/
void array_sort(uint64_t *data, const size_t len){
    size_t i, j;  
    for (i = 0; i < len-1; i++){
        // Last i elements are already in place  
        for (j = 0; j < len-i-1; j++) { 
            if (data[j] > data[j+1]) { 
                array_swap(&data[j], &data[j+1]);
            }
        }
    }      
}

void array_print(uint64_t *data, const size_t len){
    size_t i;
    printf("Print Array ------------------------------\n"); 
    for(i = 0; i < len; i++){
        printf("%ld ", data[i]); 
    }
    printf("\n");
}