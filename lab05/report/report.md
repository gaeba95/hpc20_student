# Rapport HPC - Labo 5 - Bacso Gaetan

## Sobel-bm

![](img/sobel_bm_1.png)

## Pref

```bash
❯ sudo perf report --stdio --dsos=sobel
# To display the perf.data header info, please use --header/--header-only options.
#
# dso: sobel
#
# Total Lost Samples: 0
#
# Samples: 12K of event 'cpu-clock'
# Event count (approx.): 3076750000
#
# Overhead  Command  Symbol                              
# ........  .......  ....................................
#
    35.64%  sobel    [.] sobel_filter
    27.02%  sobel    [.] gaussian_filter
     9.77%  sobel    [.] stbiw__zlib_countm
     5.87%  sobel    [.] stbi__zhuffman_decode
     3.35%  sobel    [.] stbi__parse_huffman_block
     2.57%  sobel    [.] stbi__create_png_image_raw
     2.31%  sobel    [.] stbi__fill_bits
     2.26%  sobel    [.] stbi_zlib_compress
     2.23%  sobel    [.] stbiw__paeth
     1.86%  sobel    [.] rgb_to_grayscale
     1.08%  sobel    [.] stbiw__encode_png_line
     0.62%  sobel    [.] stbi__zreceive
     0.59%  sobel    [.] stbi__zget8
     0.39%  sobel    [.] stbi_write_png_to_mem
     0.31%  sobel    [.] stbi__zhuffman_decode_slowpath
     0.20%  sobel    [.] stbiw__zlib_flushf
     0.14%  sobel    [.] stbiw__zhash
     0.11%  sobel    [.] stbiw__zlib_bitrev.constprop.3
     0.08%  sobel    [.] stbi__bitreverse16
     0.07%  sobel    [.] stbi__zbuild_huffman
     0.07%  sobel    [.] stbiw__crc32
     0.05%  sobel    [.] stbi__bit_reverse
     0.02%  sobel    [.] stbi__zbuild_huffman.constprop.2
     0.02%  sobel    [.] stbiw__zlib_bitrev.constprop.0
     0.01%  sobel    [.] memset@plt
     0.01%  sobel    [.] stbi__compute_huffman_codes
     0.01%  sobel    [.] stbi__zreceive.constprop.2
     0.01%  sobel    [.] stbiw__zlib_bitrev.constprop.1
     0.01%  sobel    [.] stbiw__zlib_bitrev.constprop.2


# Samples: 1K of event 'faults'
# Event count (approx.): 91562
#
# Overhead  Command  Symbol                        
# ........  .......  ..............................
#
    26.86%  sobel    [.] stbi__create_png_image_raw
    24.44%  sobel    [.] stbi__parse_huffman_block
     9.16%  sobel    [.] gaussian_filter
     8.97%  sobel    [.] sobel_filter
     8.91%  sobel    [.] rgb_to_grayscale
     0.24%  sobel    [.] stbiw__zlib_flushf


#
# (Tip: Use parent filter to see specific call path: perf report -p <regex>)
#

```

## Cachegrind

```
❯ valgrind --tool=callgrind --simulate-cache=yes ./sobel images/stars.png images/edge_stars.png
==15976== Callgrind, a call-graph generating cache profiler
==15976== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==15976== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==15976== Command: ./sobel images/stars.png images/edge_stars.png
==15976== 
--15976-- warning: L3 cache found, using its data for the LL simulation.
==15976== For interactive control, run 'callgrind_control -h'.
==15976== 
==15976== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==15976== Collected : 17039285910 4518679734 908001553 1814 75584965 71454713 1795 74514931 71409309
==15976== 
==15976== I   refs:      17,039,285,910
==15976== I1  misses:             1,814
==15976== LLi misses:             1,795
==15976== I1  miss rate:           0.00%
==15976== LLi miss rate:           0.00%
==15976== 
==15976== D   refs:       5,426,681,287  (4,518,679,734 rd + 908,001,553 wr)
==15976== D1  misses:       147,039,678  (   75,584,965 rd +  71,454,713 wr)
==15976== LLd misses:       145,924,240  (   74,514,931 rd +  71,409,309 wr)
==15976== D1  miss rate:            2.7% (          1.7%   +         7.9%  )
==15976== LLd miss rate:            2.7% (          1.6%   +         7.9%  )
==15976== 
==15976== LL refs:          147,041,492  (   75,586,779 rd +  71,454,713 wr)
==15976== LL misses:        145,926,035  (   74,516,726 rd +  71,409,309 wr)
==15976== LL miss rate:             0.6% (          0.3%   +         7.9%  )
```

Sans prefetcher:

```
❯ valgrind --tool=callgrind --simulate-cache=yes ./sobel images/stars.png images/edge_stars.png
==16449== Callgrind, a call-graph generating cache profiler
==16449== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==16449== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16449== Command: ./sobel images/stars.png images/edge_stars.png
==16449== 
--16449-- warning: L3 cache found, using its data for the LL simulation.
==16449== For interactive control, run 'callgrind_control -h'.
==16449== 
==16449== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==16449== Collected : 17039285910 4518679734 908001553 1814 75584965 71454713 1795 74514931 71409309
==16449== 
==16449== I   refs:      17,039,285,910
==16449== I1  misses:             1,814
==16449== LLi misses:             1,795
==16449== I1  miss rate:           0.00%
==16449== LLi miss rate:           0.00%
==16449== 
==16449== D   refs:       5,426,681,287  (4,518,679,734 rd + 908,001,553 wr)
==16449== D1  misses:       147,039,678  (   75,584,965 rd +  71,454,713 wr)
==16449== LLd misses:       145,924,240  (   74,514,931 rd +  71,409,309 wr)
==16449== D1  miss rate:            2.7% (          1.7%   +         7.9%  )
==16449== LLd miss rate:            2.7% (          1.6%   +         7.9%  )
==16449== 
==16449== LL refs:          147,041,492  (   75,586,779 rd +  71,454,713 wr)
==16449== LL misses:        145,926,035  (   74,516,726 rd +  71,409,309 wr)
==16449== LL miss rate:             0.6% (          0.3%   +         7.9%  )
```

## Callgrind

Lors du profiling avec callgrind je peux également voir que les fonctions qui sont problématiques sont sobel_filter et gaussian_filter.

## Parties sensibles

#### sobel_filter

```c
Gx += h_kernel[ky * SOBEL_KERNEL_SIZE + kx] *
      img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];

Gy += v_kernel[ky * SOBEL_KERNEL_SIZE + kx] *
      img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];
```

#### gaussian_filter

```c
pix_acc += kernel[ky * GAUSSIAN_KERNEL_SIZE + kx] *
		   img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];
```

On peut également remarquer qu'il y a des caches miss dans ces deux fonctions et lors de l'écriture de l'image.

On peut également voir que les deux fonctions du dessus perdent souvent du temps sur des instructions de type mov. Cela est surement dû au cache et à la mémoire qui n'ont pas forcément toutes les données au bon moment.

## Idées d'améliorations

On pourrait faire en sorte de toujours avoir l'image de référence en mémoire ce qui diminuerait le temps d'exectution des filtres.  Il serait aussi possible de réelement allouer la mémoire au démarrage du programme pour les nouvelles images et du coups d'avoir un accès plus rapide. (mlockall)

Il serait possible de traverser les matrices via les lignes et non via les colonnes. (Row-major)

Exemple :

```c
for (size_t x = 0; x < img->height; x++) {
        for (size_t y = 0; y < img->width; y++) {
```

Cela permet une meilleurs localité en cache. Car le prefetcher va surement prendre une ligne au dessus ou au dessous ce qui fera moins de donnée à charger lors des accès.

Il serait aussi possible de multithreader les filtres. En faisant attention au variables partagée pour ne pas avoir de perte de temps lors de l'accès au cache.  (False sharing)

Certaines idées sont inspirée de la video du cours : https://vimeo.com/97337258

