# Rapport HPC - Laboratoire 8 - Bacso Gaetan

## Introduction

Le programme qu'il nous est demandé d'optimiser génère une fractale. Il nous est demandé d'améliorer l'algorithme grâce aux instruction SIMD. Pour ce faire nous générerons 4 images au lieu d'une et les fusionnerons à la fin pour n'en avoir plus qu'une seule mais de meilleures qualités.

## Analyse des performances

### Goulets d'étranglements

Grâce à la commande perf j'ai pu identifier les différents points critiques de l'application. J'ai donc pu déterminer les points importants à optimiser avec les instructions SIMD. 

Les deux fonctions prenant le plus de temps étant la fonction affine_transform() et la fonction xorshift32(). Il est donc important de convertir ces fonctions en SIMD.

J'ai également remarqué que la condition dans la boucle de la fonction principale et le stockage des points prenait un certain temps mais que cela était moins critique au niveau performance. Je les ai cependant également convertit en SIMD par la suite car c'était tout de même une optimisation. 

### Benchmark

![](img/bench_sans_simd.png)

## Implémentation normal versus SIMD

### Goulets d'étranglements

Grâce à la commande perf j'ai analysé les goulets d'étranglement une fois le programme adapté aux instructions SIMD. J'ai pu remarquer que les goulets d'étranglement étaient plus important cette fois-ci lors de la comparaison et du stockage de l'image.

## Comparaison des performances finales

![](img/bench_avec_simd.png)

On peut remarquer que le temps d'éxecution est plus lent que lorsqu'il n'y avait pas les instruction SIMD. En revanche le nombre de point calculé est de 252M/s contre 121M/s, ce qui est une bonne amélioration car l'on a augmenté les performance par deux.

En effet le programme prend plus de temps à s'éxecuter mais il calcule 4 fois plus de temps ce qui le rend plus performant.

## Conclusion

Les instructions SIMD sont très pratiques si l'on veut optimiser du code en parallélisant le travail et s'il utilise des nombres flottants. L'éxecution des instructions n'est pas plus rapide mais permet de faire plus de choses à la fois. 


