/* 06.05.2020 - Sydney Hauke - HPC - REDS - HEIG-VD */

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#include <emmintrin.h>


#include "image.h"
#include "ifs.h"
#include "random.h"

#define TRANSFORM_SIZE      6

/* Sierpinski triangle */
#define NB_ST_TRANSFORMS    3
float ST_TRANSFORMS[NB_ST_TRANSFORMS][TRANSFORM_SIZE] = {
{
    0.5f,  0.0f,   0.0f,
    0.0f,  0.5f,   0.0f,
},
{
    0.5f,  0.0f,   0.5f,
    0.0f,  0.5f,   0.0f,
},
{
    0.5f,  0.0f,   0.25f,
    0.0f,  0.5f,   0.5f,
}};

/* Barnsley's fern */
#define NB_BF_TRANSFORMS    4
float BF_TRANSFORMS[NB_BF_TRANSFORMS][TRANSFORM_SIZE] = {
{
    0.0f,  0.0f,   0.0f,
    0.0f,  0.16f,   0.0f,
},
{
    0.2f,  -0.26f,   0.0f,
    0.23f,  0.22f,   1.6f,
},
{
    -0.15f,  0.28f,   0.0f,
    0.26f,  0.24f,   0.44f,
},
{
    0.85f,  0.04f,   0.0f,
    -0.04f,  0.85f,   1.6f,
}};

struct ifs_t {
    float width;
    float height;

    float center_x;
    float center_y;

    size_t nb_transforms;
    float *transforms;
};

/* Sierpinski triangle IFS */
const struct ifs_t st_ifs = {
    .width  = 1.0f,
    .height = 1.0f,

    .center_x = 0.5f,
    .center_y = 0.5f,

    .nb_transforms = NB_ST_TRANSFORMS,
    .transforms = (float*)ST_TRANSFORMS,
};

/* Barnley's fern IFS */
const struct ifs_t bf_ifs = {
    .width  = 6.0f,
    .height = 10.0f,

    .center_x = 0.0f,
    .center_y = 4.5f,

    .nb_transforms = NB_BF_TRANSFORMS,
    .transforms = (float*)BF_TRANSFORMS, 
};

static void affine_transform(__m128 *x, __m128 *y, const __m128 transform[6]);
static size_t __ifs(struct img_t *img, const struct ifs_t *ifs, size_t passes, size_t width, size_t height);

void ifs(char *pathname, size_t passes, size_t min_width)
{
    const struct ifs_t *i = &bf_ifs;
    struct img_t *fractal_img;
    size_t width, height;
    float aspect_ratio;

    /* Fractals have a specific aspect ratio. The resulting image
     * must have the same aspect ratio as well */
    aspect_ratio = i->height / i->width;
    width = min_width;
    height = width * aspect_ratio;

    fractal_img = allocate_image(width, height, COMPONENT_GRAYSCALE);

    /* Generate fractal image */
    size_t points_drawn = __ifs(fractal_img, i, passes, width, height);
    printf("Number of points drawn : %lu\n", points_drawn);

    save_image(pathname, fractal_img);
    printf("Fractal saved as %s (%lu, %lu)\n", pathname, width, height);

    free_image(fractal_img);
}

static size_t __ifs(struct img_t *img, const struct ifs_t *ifs, size_t passes, size_t width, size_t height)
{
    /* TODO : do multiple instances of this algorithm so that the number of points generated 
     * per second increases. Use SIMD instructions. */

    /* This is the real number of iterations we do to draw the fractal */
    size_t count_points = width*height*passes;

    /* We start from the origin point */
    __m128 p_x = _mm_set1_ps(0.0f);
    __m128 p_y = _mm_set1_ps(0.0f);
    __m128 index_y_v;
    __m128 index_x_v;
    __m128 index_v;
    __m128 trans_v[6];
    __m128 test_v;
    float* transform[4];
    __m128 xtmp;
    __m128 ytmp;
    /* Choose a random starting state for the random number generator */
     __m128i rand_v = _mm_set_epi32(rand(),rand(),rand(),rand());

    srand(time(NULL));

    float width_moins = ifs->center_x-ifs->width/2;
    float height_moins = ifs->center_y-ifs->height/2;

    for (size_t iterations = count_points; iterations != 0; iterations--) {
        /* Randomly choose an affine transform */
        rand_v = _mm_xor_si128(_mm_slli_epi32(rand_v,13),rand_v);
        rand_v = _mm_xor_si128(_mm_srli_epi32(rand_v,17),rand_v);
        rand_v = _mm_xor_si128(_mm_slli_epi32(rand_v,5),rand_v);
        
        transform[0] = &ifs->transforms[rand_v[0]%ifs->nb_transforms*TRANSFORM_SIZE];
        transform[1] = &ifs->transforms[rand_v[1]%ifs->nb_transforms*TRANSFORM_SIZE];
        transform[2] = &ifs->transforms[rand_v[2]%ifs->nb_transforms*TRANSFORM_SIZE];
        transform[3] = &ifs->transforms[rand_v[3]%ifs->nb_transforms*TRANSFORM_SIZE];

        for(int i = 0; i < 6; i++){
            trans_v[i] = _mm_set_ps(transform[0][i],transform[1][i],transform[2][i],transform[3][i]);
        }


        xtmp = p_x;
        ytmp = p_y;
        p_x = _mm_add_ps((_mm_add_ps(_mm_mul_ps(xtmp,trans_v[0]),_mm_mul_ps(ytmp,trans_v[1]))),trans_v[2]);
        p_y = _mm_add_ps((_mm_add_ps(_mm_mul_ps(xtmp,trans_v[3]),_mm_mul_ps(ytmp,trans_v[4]))),trans_v[5]);
        /* Apply choosen affine transform */
       // affine_transform(&p_x, &p_y, trans_v);

        /* Check if point lies inside the boundaries of our image */

        test_v = _mm_and_ps(_mm_and_ps(_mm_and_ps(_mm_cmplt_ps(p_x,_mm_set1_ps(ifs->center_x+ifs->width/2)),
                                 _mm_cmpgt_ps(p_x,_mm_set1_ps(ifs->center_x-ifs->width/2))),
                                 _mm_cmplt_ps(p_y,_mm_set1_ps(ifs->center_y+ifs->height/2))),
                                 _mm_cmpgt_ps(p_y,_mm_set1_ps(ifs->center_y-ifs->height/2)));

            
        index_y_v = _mm_sub_ps(p_y,_mm_set1_ps(height_moins));
        index_y_v = _mm_div_ps(index_y_v,_mm_set1_ps(ifs->height));
        index_y_v = _mm_mul_ps(index_y_v,_mm_set1_ps(height));
        index_y_v = _mm_floor_ps(index_y_v);
        index_y_v = _mm_sub_ps(_mm_set1_ps(height-1),index_y_v);
        index_y_v = _mm_mul_ps(index_y_v,_mm_set1_ps(width));
        
        index_x_v = _mm_sub_ps(p_x,_mm_set1_ps(width_moins));
        index_x_v = _mm_div_ps(index_x_v,_mm_set1_ps(ifs->width));
        index_x_v = _mm_mul_ps(index_x_v,_mm_set1_ps(width));

        index_v = _mm_add_ps(index_y_v,index_x_v);

        for(int i = 0; i < 4; i++){
        /* If point lies in the image, save and colour it in white */
            if((int)test_v[i]){
                img->data[(int)index_v[i]] = UINT8_MAX;
            }
            
        }
        
    }

    return (count_points*4);
}