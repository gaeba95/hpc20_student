#pragma once

#define KB (1 << 10)
#define MB (1 << 20)

/* Customizable */
#define RANDOM_MAX 10

#define N 1

#define SIZE 10000

#define RUNS 5
#define ITERATIONS 10

