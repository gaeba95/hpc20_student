#!/bin/zsh
echo "---------------- Start benchmark ----------------"
echo "---------------- Start time benchmark ----------------"
\time -o time-list.txt -pv ./sort list 1000000
\time -o time-array.txt -pv ./sort array 1000000
echo "---------------- Start strace benchmark ----------------"
strace -tt -o strace-list.txt ./sort list 1000000
strace -tt -o strace-array.txt ./sort array 1000000
echo "---------------- Start gprof benchmark ----------------"
./sort list 1000000
gprof sort gmon.out > gprof-list.txt
./sort array 1000000
gprof sort gmon.out > gprof-array.txt
echo "---------------- Finish ----------------"
