/* 26.01.16 - Xavier Ruppen - HPC - REDS - HEIG-VD */

#include <stdio.h>
#include <stdlib.h>
#include "benchmark/benchmark.h"
#include "bench.h"

extern "C" {
#include "../include/array_util.h"
#include "../include/list_util.h"
}

class SortFixture : public benchmark::Fixture {
public :
    void SetUp(const ::benchmark::State&)
    {
        array = array_init(SIZE);
        list = list_init(SIZE);
    }

    void TearDown(const ::benchmark::State&)
    {
        list_clear(list);
        array_clear(array);
    }

protected :
    uint64_t * array;
    struct list_element * list;
};

void BM_spin_empty(benchmark::State& state) {
  for (auto _ : state) {
    for (int x = 0; x < state.range(0); ++x) {
      benchmark::DoNotOptimize(x);
    }
  }
}


BENCHMARK_F(SortFixture, array_sort)(benchmark::State& state) {
    while (state.KeepRunning()) {

        array_sort(array, SIZE);
        array_clear(array);
        array = array_init(SIZE);
    }
}

BENCHMARK_F(SortFixture, list_sort)(benchmark::State& state) {
    while (state.KeepRunning()) {

        list_sort(list);
        list_clear(list);
        list = list_init(SIZE);
    }
}

BENCHMARK_REGISTER_F(SortFixture, array_sort)
  ->ComputeStatistics("max", [](const std::vector<double>& v) -> double {
    return *(std::max_element(std::begin(v), std::end(v)));
});

BENCHMARK_REGISTER_F(SortFixture, list_sort)
  ->ComputeStatistics("max", [](const std::vector<double>& v) -> double {
    return *(std::max_element(std::begin(v), std::end(v)));
});

BENCHMARK_REGISTER_F(SortFixture, array_sort)
  ->ComputeStatistics("min", [](const std::vector<double>& v) -> double {
    return *(std::min_element(std::begin(v), std::end(v)));
});

BENCHMARK_REGISTER_F(SortFixture, list_sort)
  ->ComputeStatistics("min", [](const std::vector<double>& v) -> double {
    return *(std::min_element(std::begin(v), std::end(v)));
});

BENCHMARK_MAIN();
