
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "../include/list_util.h"
/* A list_element must at least contain a link to the next
 * element, as well as a uint64_t data value */
struct list_element{
    int val;
    struct list_element * next;
};

/* Allocate "len" linked elements and initialize them
 * with random data.
 * Return list head */
struct list_element *list_init(size_t len){
    size_t i;
    struct list_element * head = (struct list_element*) malloc(sizeof(struct list_element));
    struct list_element * actual = head;
    srand(time(NULL));
    actual->val = rand() % 100;
    for(i = 1; i < len; i++){
        actual->next = (struct list_element*) malloc(sizeof(struct list_element));
        actual = actual->next;
        actual->val = rand() % 100;
    }
    actual->next = NULL;
    return head;
}

/* Liberate list memory */
void list_clear(struct list_element *head){
    struct list_element * actual = head;
    while(actual->next != NULL){
        actual = actual->next;
        free(head);
        head = actual;
    }
    free(actual);
}

// https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
/* function to swap data of two nodes a and b*/
void list_swap(struct list_element *a, struct list_element *b) 
{ 
    int temp = a->val; 
    a->val = b->val; 
    b->val = temp; 
} 

// https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
/* Arrange a list in increasing order of value */
void list_sort(struct list_element *head){
    size_t swapped; 
    struct list_element *ptr1; 
    struct list_element *lptr = NULL; 
  
    /* Checking for empty list */
    if (head == NULL) 
        return; 
  
    do
    { 
        swapped = 0; 
        ptr1 = head; 
  
        while (ptr1->next != lptr) 
        { 
            if (ptr1->val > ptr1->next->val) 
            {  
                list_swap(ptr1, ptr1->next); 
                swapped = 1; 
            } 
            ptr1 = ptr1->next; 
        } 
        lptr = ptr1; 
    } 
    while (swapped); 
}

// https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
void list_print(struct list_element *head){
    struct list_element *tmp = head; 
    printf("Print List ------------------------------\n"); 
    while (tmp!=NULL) 
    { 
        printf("%d ", tmp->val); 
        tmp = tmp->next; 
    } 

    printf("\n");
}